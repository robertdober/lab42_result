Warning[:experimental] = false
module Lab42
  class Result
    attr_reader :status, :value
    def deconstruct
      [status, value].freeze
    end

    def if_error &blk
      blk.(status, value) unless ok?
    end

    def if_ok &blk
      blk.(value) if ok?
    end

    def ok?
      status == :ok
    end

    def raise!(alternative_exception=nil, &blk)
      return value if ok?
      message = blk ? blk.(value) : value
      raise((alternative_exception||status), message)
    end

    class << self
      def new(*,**)
        raise NoMethodError, "only use the constructors `.ok` and `.error`"
      end

      def error(message, error: RuntimeError)
        raise ArgumentError, "#{error.inspect} must be an Exception" unless Exception.class === error
        o = allocate
        o.instance_exec do
          @status = error
          @value  = message
        end
        o.freeze
      end

      def from(error_message, &blk)
        ok(blk.())
      rescue Exception => e
        error(error_message, error: e.class)
      end

      def ok(value=nil)
        o = allocate
        o.instance_exec do
          @status = :ok
          @value  = value
        end
        o.freeze
      end
    end

  end
end
