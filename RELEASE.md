# 0.1.5 2020-12-04

- added `lab42/result/autoimport`

- added conditional execution methods `if_ok` and `if_error`

# 0.1.4 2020-12-02

- added block to allow access to the original message in `raise!` 

# 0.1.3 2020-12-02

- added optional custom exception to `raise!` 

# 0.1.2 2020-11-15

identical to 0.1.1 gem push hiccup

# 0.1.1 2020-11-15


Added `Result.from` 

# 0.1.0 2020-11-14
