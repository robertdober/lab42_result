$:.unshift( File.expand_path( "../lib", __FILE__ ) )
require "lab42/result/version"
version = Lab42::Result::VERSION
Gem::Specification.new do |s|
  s.name        = 'lab42_result'
  s.version     = version
  s.summary     = 'Result à la Either (of Haskell)'
  s.description = %{Result instances are immutable objects representing an error with a message or an absence of error with a value}
  s.authors     = ["Robert Dober"]
  s.email       = 'robert.dober@gmail.com'
  s.files       = Dir.glob("lib/**/*.rb")
  s.files      += %w{LICENSE README.md}
  s.homepage    = "https://bitbucket.org/robertdober/lab42_result"
  s.licenses    = %w{Apache-2.0}

  s.required_ruby_version = '>= 2.7.0'
  # s.add_dependency 'forwarder2', '~> 0.2'

  s.add_development_dependency 'pry', '~> 0.10'
  s.add_development_dependency 'pry-byebug', '~> 3.9'
  s.add_development_dependency 'rspec', '~> 3.10'
  # s.add_development_dependency 'travis-lint', '~> 2.0'
end
