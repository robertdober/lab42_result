require "lab42/result/version"
RSpec.describe "Version #{Lab42::Result::VERSION}" do
  version = description

  let(:semantic) { %r{ \A Version \s \d+ \. \d+ \. \d+ (?:\-[[:alnum:]]+)? \z }x }

  it "is semantic" do
    expect( version ).to match(semantic)
  end
end
